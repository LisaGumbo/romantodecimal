package RomanToDecimal;

public class RomanToDecimal {
    public Integer getDecimal(String roman){
        if(roman == "I" || roman =="i"){
            return 1;
        }else if(roman == "V" || roman == "v"){
            return 5;
        }else if(roman == "X" || roman == "x"){
            return 10;
        }else if(roman == "L" || roman == "l"){
            return 50;
        }else if(roman == "C" || roman == "c"){
            return 100;
        }else if(roman == "D" || roman == "d"){
            return 500;
        }else
            return 1000;
    }
}
