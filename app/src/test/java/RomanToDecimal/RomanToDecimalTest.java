package RomanToDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RomanToDecimalTest {

    RomanToDecimal romanToDecimal;

    @BeforeEach
    public void testSetup(){
        romanToDecimal = new RomanToDecimal();
    }

    @Test
    public void getDecimalFromRoman(){
        assertEquals(1,romanToDecimal.getDecimal("I"));
        assertEquals(5,romanToDecimal.getDecimal("V"));
        assertEquals(10,romanToDecimal.getDecimal("X"));
        assertEquals(50,romanToDecimal.getDecimal("L"));
        assertEquals(100,romanToDecimal.getDecimal("C"));
        assertEquals(500,romanToDecimal.getDecimal("D"));
        assertEquals(1000,romanToDecimal.getDecimal("M"));
    }
    @Test
    public void roman4(){
        assertEquals(4,romanToDecimal.getDecimal("IV"));
    }

}
